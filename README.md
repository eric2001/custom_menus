# Custom Menus
![Custom Menus](./screenshot.jpg) 

## Description
Custom menus is a module for Gallery 3 that allows admin's to add additional menu items to the menu bar at the top of the page, after the "Home" link. These menu items can link to internal or external pages and can have sub items of their own.

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/102814).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "custom_menus" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.

To create new menu's, select Admin -> content -> Custom Menus Manager, and then click the add new menu link. Enter in a title and a URL for the menu item to link to, then press Create menu (if this menu item is to contain sub-items, leave the URL field blank). To add sub menu's, press the "+" link next to an existing menu. Use the "^" and "v" links to move a menu item up or down one position.  

## History
**Version 1.2.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 28 August 2021.
>
> Download: [Version 1.2.0](/uploads/0f483fe662f1073d71ceba435547cfdd/custom_menus120.zip)

**Version 1.1.0:**
> - Updated to allow menu items to be translated into other languages.
> - Released 21 July 2011.
>
> Download: [Version 1.1.0](/uploads/8a8d4f333a768f7b29b1f85564ecfdbc/custom_menus110.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 04 July 2011.
>
> Download: [Version 1.0.0](/uploads/44a5e5dad3c28adbe334b2d3eeaeefdc/custom_menus100.zip)
